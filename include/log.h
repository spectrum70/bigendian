#ifndef log_h
#define log_h

#include <stdarg.h>

void inf(char *msg, ...);
void msg(char *msg, ...);
void dbg(char *dbg, ...);
void err(char *msg, ...);

void logmsg(char *msg, ...);

#endif /* log_h */
