#ifndef config_h
#define config_h

#include <stdint.h>
#include <objects.h>

#define MAX_CHANNELS	64
#define MAX_ACTIONS	64

#define MAX_OWNER_IDS	16

enum event {
	EVT_NONE,
	EVT_JOIN,
	EVT_MESG,
};

enum action {
	ACT_NONE,
	ACT_FIND_CI,
};

struct evt_actions {
	char ifind[512];
	char reply[2048];
};

struct channel {
	char name[128];
	enum event evt[MAX_ACTIONS];
	struct evt_actions act[MAX_ACTIONS];
};

struct config {
	struct channel channels[MAX_CHANNELS];
	int64_t allow_list[MAX_OWNER_IDS];
};

int config_read_file();
char * config_get_chatgroup_topic(char *title);
char * config_special_tag_replace(struct new_chat_member *ncm, char *haystack);

#endif /* config_h */
