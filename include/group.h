#ifndef group_h
#define group_h

#include <stdint.h>

#define MAX_TITLE	32

void group_update_id_table(int64_t chat_id, char *chat_title);
int64_t group_get_id(int idx);

struct chat_info {
	int64_t id;
	char title[MAX_TITLE];
};

#endif // group_h

