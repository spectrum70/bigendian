#ifndef commands_h
#define commands_h

#include <objects.h>

typedef int(*cmd_callback)(struct update *u, char *reply, int64_t *dest_id);

#define CF_ENABLED		0x01
#define CF_HTML_REPLY		0x02
#define CF_USER_CHECK		0x04

struct command {
	char *cmd;
	cmd_callback cb;
	int flags;
};

#define MAX_CMD_LEN		20
#define MAX_ARG_LEN		32
#define MAX_CMD_ARGS_LEN	512

extern const struct command cmds_pvt[];
extern const struct command cmds_grp[];

#endif /* commands_h */
