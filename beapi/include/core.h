#ifndef __core_h
#define __core_h

#include "objects.h"

#define MAX_TOKEN	64

int be_init();
void be_exit();
void be_setup_token(char *token);

int be_get_updates(struct update *update);
int be_send_message(int64_t chat_id, char *message, uint8_t html);
int be_send_message_in_reply_to(int64_t chat_id, int64_t to_id,
				char *message, uint8_t html);
void be_cleanup_updates(struct update *root);
int be_kick_chat_member(int64_t chat_id, int64_t user_id);

#endif /* __core_h */
