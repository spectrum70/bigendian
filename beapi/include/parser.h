#ifndef __parser_h
#define __parser_h

struct update;

int be_get_last_update_id();
void be_json_parse_update(void * ptr, struct update *update);

#endif /* __parser_h */
