#ifndef __objects_h
#define __objects_h

#include <inttypes.h>

#define MAX_FIRST_NAME		32
#define MAX_LAST_NAME		MAX_FIRST_NAME
#define MAX_USERNAME		32
#define MAX_TITLE		32
#define MAX_LANGUAGE_CODE	8
#define MAX_TEXTMSG		1024

struct from_user {
	int64_t id;
	uint8_t is_bot;
	char first_name[MAX_FIRST_NAME];
	char username[MAX_USERNAME];
	char language_code[MAX_LANGUAGE_CODE];
};

struct new_chat_member {
	int64_t id;
	char first_name[MAX_FIRST_NAME];
	char last_name[MAX_LAST_NAME];
	char username[MAX_USERNAME];
};

struct message {
	int64_t id;
	uint64_t date;
	uint8_t edited;
	struct chat *chat;
	struct from_user *from;
	struct new_chat_member *new_chat_member;
	char text[MAX_TEXTMSG];
	struct message *reply_to;
};

enum chat_type {
	ct_none,
	ct_supergroup,
};

struct chat {
	int64_t id;
	enum chat_type type;
	char username[MAX_USERNAME];
	char title[MAX_TITLE];
};

struct update {
	uint8_t result;
	int64_t update_id;
	struct message *msg;
	struct update *next;
};

#endif /* _objects_h */
