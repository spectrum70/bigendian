/*
 * Simple C Telegram Bot api
 *
 * Angelo Dureghello (C) 2019 <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <curl/curl.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "core.h"
#include "parser.h"
#include "objects.h"

#define MAX_URL		(1024 * 32)
#define MAX_REQUEST	(1024 * 16)
#define api_url		"https://api.telegram.org/bot"

struct context {
	CURL *curl;
	char request_url[MAX_URL];
};

struct context *ctx;

static char rfc3986[256];
static char html5[256];
static char request[MAX_REQUEST];
static char encoded[MAX_REQUEST];

static size_t __be_get_reply(void *ptr, size_t size, size_t nmemb, void *data)
{
	struct update *update = (struct update *)data;
	int tot_size = size * nmemb;

	if (!update)
		return 0;

	printf("reply: %s\n", (char *)ptr);

	be_json_parse_update(ptr, update);

	return tot_size;
}

static int __be_curl_request(char *request, void *data)
{
	int rval;
	char url[MAX_URL] = {0};

	if (!ctx->curl)
		return -1;

	strcat(url, ctx->request_url);
	strcat(url, request);

	curl_easy_setopt(ctx->curl, CURLOPT_URL, url);
	curl_easy_setopt(ctx->curl, CURLOPT_SSL_VERIFYPEER, 0L);

	if (data) {
		curl_easy_setopt(ctx->curl,
			CURLOPT_WRITEFUNCTION, __be_get_reply);
		curl_easy_setopt(ctx->curl, CURLOPT_WRITEDATA, data);
	}

	/* Perform the request, res will get the return code */
	rval = curl_easy_perform(ctx->curl);
	if (rval != CURLE_OK) {
		printf("err: %s\n", curl_easy_strerror(rval));
		return -2;
	}

	return 0;
}

static void __be_url_encoder_rfc_tables_init()
{
	int i;

	for (i = 0; i < 256; i++) {
		rfc3986[i] = isalnum(i)||i == '~'||i == '-'||i == '.'||i == '_'
			? i : 0;
		html5[i] = isalnum(i)||i == '*'||i == '-'||i == '.'||i == '_'
			? i : (i == ' ') ? '+' : 0;
	}
}

/*
 * Caller responsible for memory
 */
static void __be_url_encode(const char *s, char *enc, char *tb)
{
	for (; *s; s++) {
		int c = (int)*s;

		if (tb[c])
			sprintf(enc, "%c", tb[c]);
		else
			sprintf(enc, "%%%02X", *s);

		while (*++enc)
			;
	}
}

int be_init()
{
	curl_global_init(CURL_GLOBAL_DEFAULT);

	if (!ctx)
		ctx = (struct context *)calloc(1, sizeof(struct context));

	if (!ctx)
		return -1;

	if (ctx->curl)
		be_exit();

	__be_url_encoder_rfc_tables_init();

	ctx->curl = curl_easy_init();

	return 0;
}

void be_exit()
{
	curl_easy_cleanup(ctx->curl);
	curl_global_cleanup();
}

void be_setup_token(char *token)
{
	if (!ctx)
		return;

	if (!token) {
		memset(ctx->request_url, 0, MAX_TOKEN);
	} else {
		strcat(ctx->request_url, api_url);
		strcat(ctx->request_url, token);
	}
}

static int __be_send_message(char *request, char *message, uint8_t html)
{
	if (html)
		strcat(request, "&parse_mode=html");

	strcat(request, "&text=");

	if (message) {
		__be_url_encode(message, encoded, rfc3986);
		strcat(request, encoded);
	}

	return __be_curl_request(request, 0);
}

int be_send_message(int64_t chat_id, char *message, uint8_t html)
{
	sprintf(request, "/sendMessage?chat_id=%ld", chat_id);

	return __be_send_message(request, message, html);
}

int be_send_message_in_reply_to(int64_t chat_id, int64_t to_id,
	char *message, uint8_t html)
{
	sprintf(request, "/sendMessage?chat_id=%ld&reply_to_message_id=%ld",
		chat_id, to_id);

	return __be_send_message(request, message, html);
}

int be_get_updates(struct update *update)
{
	char request[64] = {0};
	int id;

	/*
	 * Updates are acknowledged sending offset of last processed + 1.
	 */
	id = be_get_last_update_id();
	id++;

	/*
	 * Ha, json_object_object_foreach seems to segfault if the passed
	 * ptr has a string > 16K. To go deeper and find a fix in case.
	 * For now, let's limit updates to 20.
	 */
	sprintf(request, "/getUpdates?limit=20&offset=%d", id);

	return __be_curl_request(request, update);
}

int be_kick_chat_member(int64_t chat_id, int64_t user_id)
{
	sprintf(request, "/kickChatMember?chat_id=%ld&user_id=%ld",
		chat_id, user_id);

	return __be_send_message(request, 0, 0);
}

void be_cleanup_updates(struct update *root)
{
	struct update *u = root, *next;

	for (;;) {
		/* get next, always */
		next = u->next;

		if (u->msg) {
			if (u->msg->from)
				free(u->msg->from);
			if (u->msg->chat)
				free(u->msg->chat);
			if (u->msg->new_chat_member)
				free(u->msg->new_chat_member);
			free(u->msg);
		}
		if (u != root)
			free(u);
		if (!next)
			break;
		u = next;
	}
}
