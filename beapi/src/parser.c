/*
 * Simple C Telegram Bot api
 *
 * Angelo Dureghello (C) 2019 <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <stdio.h>
#include <string.h>
#include <json-c/json.h>
#include "parser.h"
#include "objects.h"

//#define dbg_json

#define c_update	((struct update *)pctx.data)
#define new_update 	calloc(1, sizeof(struct update));
#define new_message 	calloc(1, sizeof(struct message));
#define new_chat	calloc(1, sizeof(struct chat));
#define new_chat_mem	calloc(1, sizeof(struct new_chat_member));
#define new_from	calloc(1, sizeof(struct from_user));

#define MAX_STATE_STACK		16

#if defined(dbg_json)
	#define color_obj "\x1b[33;1m"
	#define color_arr "\x1b[34;1m"
	#define color_rst "\x1b[0m"
	#define dbgj printf
#else
	#define dbgj(...)
#endif

enum pc_type {
	pct_update,
};

struct parse_context {
	int type;
	void *data;
	int last_update_id;
};

enum {
	st_none,
	st_chat,
	st_message,
	st_new_chat_member,
	st_from,
};

static int stack[MAX_STATE_STACK];
static int ptr = 0;
static struct parse_context pctx;

static void be_json_parse_object(json_object *jobj);

/* reply: {"ok":true,"result":
[{"update_id":32798584, "message":
	{
	"message_id":82,
	"from":
		{"id":201672292,"is_bot":false,"first_name":"angelo","username":"a_ng_elo","language_code":"en"},
	"chat":
		{"id":-337413545,"title":"testgroup-angelo","type":"group","all_members_are_administrators":true},
	"date":1585518558,
	"new_chat_participant":
		{"id":754380115,"is_bot":true,"first_name":"bigendian","username":"bigendian_bot"},
	"new_chat_member":
		{"id":754380115,"is_bot":true,"first_name":"bigendian","username":"bigendian_bot"},
	"new_chat_members":
		[{"id":754380115,"is_bot":true,"first_name":"bigendian","username":"bigendian_bot"}]}}]}
*/

static void be_store_chat_str(char *key, const char *str)
{
	dbgj("be_store_chat_str() %s %s\n", key, str);

	if (!c_update || !c_update->msg || !c_update->msg->chat)
		return;

	if (!strcmp(key, "title"))
		strcat(c_update->msg->chat->title, str);
	else if (!strcmp(key, "username"))
		strcat(c_update->msg->chat->username, str);
	else if (!strcmp(key, "type")) {
		if (!strcmp(str, "supoergroup"))
			c_update->msg->chat->type = ct_supergroup;
	}
}

static void be_json_parse_array(json_object *jobj, char *key)
{
	enum json_type type;
	int array_len;
	int i;
	json_object *jvalue;
	json_object *jarray = jobj;

	if (key) {
		json_object_object_get_ex(jobj, key, &jarray);
		if (!jarray)
			return;
	}

	array_len = json_object_array_length(jarray);

	dbgj("be_json_parse_array() array length: %d\n", array_len);

	for (i = 0; i < array_len; i++) {
		jvalue = json_object_array_get_idx(jarray, i);
		type = json_object_get_type(jvalue);
		if (type == json_type_array) {
			be_json_parse_array(jvalue, NULL);
		} else if (type != json_type_object) {
			dbgj("value[%d]: \n", i);
		} else {
			dbgj(color_arr "object : %s\n" color_rst, key);
			be_json_parse_object(jvalue);
		}
	}
}

static void be_push_state(int state)
{
	stack[ptr] = state;
	if (ptr < MAX_STATE_STACK)
		ptr++;
}

static int be_pop_state()
{
	if (ptr == 0)
		return st_none;

	return stack[--ptr];
}

static void be_json_parse_object(json_object *jobj)
{
	enum json_type type;
	json_object *jobj_next;
	static struct message *msg_ptr = NULL;
	static int state = st_none;

	dbgj("be_json_parse_object() entering\n");

	/*Passing through every array element*/
	json_object_object_foreach(jobj, key, val) {
		type = json_object_get_type(val);
		switch (type) {
		case json_type_boolean:
			dbgj("bool: %s:%d\n",
				key, json_object_get_boolean(val));
			if (key[0] == 'o' && key[1] == 'k') {
				/* new reply, creating */
				int rval = json_object_get_boolean(val);
				if (pctx.type == pct_update)
					c_update->result = rval;
				if (!rval)
					return;
			}
			break;
		case json_type_double:
			dbgj("double: %f\n", json_object_get_double(val));
			break;
		case json_type_int: {
			int64_t ival = json_object_get_int64(val);
			dbgj("int: %s:%ld\n", key, ival);
			switch (state) {
			case st_none:
				if (!strcmp(key, "update_id")) {
					if (c_update->update_id) {
						c_update->next = new_update;
						pctx.data = c_update->next;
					}
					c_update->update_id = ival;
					if (ival > pctx.last_update_id)
						 pctx.last_update_id = ival;
				}
				break;
			case st_message:
				if (!strcmp(key, "message_id"))
					msg_ptr->id = ival;
				break;
			case st_chat:
				if (!strcmp(key, "id"))
					msg_ptr->chat->id = ival;
				break;
			case st_new_chat_member:
				if (!strcmp(key, "id"))
					msg_ptr->new_chat_member->id = ival;
				break;
			case st_from:
				if (!strcmp(key, "id"))
					msg_ptr->from->id = ival;
				break;
			}
			}
			break;
		case json_type_string: {
			const char *str = json_object_get_string(val);

			switch (state) {
			case st_chat:
				be_store_chat_str(key, str);
				break;
			case st_message:
				if (!strcmp(key, "text"))
					strncat(msg_ptr->text,
						str, MAX_TEXTMSG);
				break;
			case st_new_chat_member:
				if (!strcmp(key, "username")) {
					strncat(msg_ptr->
						new_chat_member->username,
						str, MAX_USERNAME);
				} else if (!strcmp(key, "first_name")) {
					strncat(msg_ptr->
						new_chat_member->first_name,
						str, MAX_FIRST_NAME);
				} else if (!strcmp(key, "last_name")) {
					strncat(msg_ptr->
						new_chat_member->last_name,
						str, MAX_LAST_NAME);
				}
				break;
			}
			}
			break;
		case json_type_object:
			dbgj(color_obj "object: %s\n" color_rst, key);
			be_push_state(state);
			if (!strcmp(key, "message") ||
				!strcmp(key, "edited_message")) {
				c_update->msg = msg_ptr = new_message;
				state = st_message;
			} else if (!strcmp(key, "reply_to_message")) {
				c_update->msg->reply_to = msg_ptr = new_message;
				state = st_message;
			} else if (!strcmp(key, "chat")) {
				if (!msg_ptr)
					 c_update->msg = msg_ptr = new_message;
				msg_ptr->chat = new_chat;
				state = st_chat;
			} else if (!strcmp(key, "new_chat_member")) {
				msg_ptr->new_chat_member = new_chat_mem;
				state = st_new_chat_member;
			} else if (!strcmp(key, "from")) {
				msg_ptr->from = new_from;
				state = st_from;
			}
			json_object_object_get_ex(jobj, key, &jobj_next);
			be_json_parse_object(jobj_next);
			break;
		case json_type_array:
			dbgj(color_arr "array : %s\n" color_rst, key);
			be_json_parse_array(jobj, key);
			break;
		case json_type_null:
			break;
		}
	}

	state = be_pop_state();

	dbgj("be_json_parse() exiting\n");
}

int be_get_last_update_id()
{
	return pctx.last_update_id;
}

/*
 * Actually handling only updates.
 */
void be_json_parse_update(void * ptr, struct update *update)
{
	json_object *jobj;

	pctx.type = pct_update;
	pctx.data = (void *)update;

	jobj = json_tokener_parse(ptr);

	if (jobj)
		be_json_parse_object(jobj);
}
