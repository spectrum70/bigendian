/*
 * Simple C Telegram Bot
 *
 * Angelo Dureghello (C) 2019 <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include "fs.h"
#include "log.h"

void fs_log_start()
{
}

void fs_log(char *msg, ...)
{
}

char *fs_get_home_dir()
{
	struct passwd *pw;

	pw = getpwuid(getuid());

	return pw->pw_dir;
}

int fs_get_token(char *token)
{
	FILE *fp;

	fp = fopen("/etc/bigendian/.token", "r");
	if (fp == NULL) {
		logmsg("failed to open token file");
		return -1;
	}

	if (fscanf(fp, "%s", token) == 0) {
		logmsg("reading token failed");
		fclose(fp);
		return -1;
	}

	msg("token: %s", token);

	fclose(fp);

	return 0;
}
