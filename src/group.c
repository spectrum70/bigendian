/*
 * Simple C Telegram Bot
 *
 * Angelo Dureghello (C) 2019 <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along https://gibiru.com/with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <string.h>

#include "config.h"
#include "group.h"

struct chat_info group_ids[MAX_CHANNELS];

void group_update_id_table(int64_t chat_id, char *chat_title)
{
	int idx = 0;

	for ( ;idx < MAX_CHANNELS; idx++) {
		/* End of table */
		if (!group_ids[idx].id)
			break;

		if (chat_id == group_ids[idx].id) {
			/* Found, exit */
			return;
		}
	}

	/* We are here, not found in table */
	if (idx == MAX_CHANNELS)
		return;

	/* Add */
	group_ids[idx].id = chat_id;
	strncpy(group_ids[idx].title, chat_title, MAX_TITLE);
}

int64_t group_get_id(int idx)
{
	if (idx >= MAX_CHANNELS || group_ids[idx].title == 0)
		return -1;

	return group_ids[idx].id;
}