/*
 * Simple C Telegram Bot
 *
 * Angelo Dureghello (C) 2019 <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <stdio.h>
#include "log.h"

#define vt100_red	"\x1b[1;31m"
#define vt100_yel	"\x1b[1;33m"
#define vt100_cya	"\x1b[1;36m"
#define vt100_rst	"\x1b[0m"

#define color_inf	vt100_yel
#define color_msg	vt100_cya
#define color_err	vt100_red
#define color_rst	vt100_rst

#define reset_crlf	color_rst "\n"

void inf(char *msg, ...)
{
	va_list ap;

	va_start(ap, msg);
	printf(color_inf);
	vprintf(msg, ap);
	va_end(ap);
	printf(reset_crlf);
}

void msg(char *msg, ...)
{
	va_list ap;

	va_start(ap, msg);
	printf(color_msg);
	vprintf(msg, ap);
	va_end(ap);
	printf(reset_crlf);
}

void dbg(char *msg, ...)
{
	va_list ap;

	va_start(ap, msg);
	vprintf(msg, ap);
	va_end(ap);
	printf(reset_crlf);
}

void err(char *msg, ...)
{
	va_list ap;

	va_start(ap, msg);
	printf(color_err);
	printf("+++ ");
	vprintf(msg, ap);
	va_end(ap);
	printf(reset_crlf);
}

void logmsg(char *msg, ...)
{
	va_list ap;
	int rval, retries;
	static FILE *f = NULL;
	
	if (!f) {
		f = fopen("/var/log/bigendian", "a+");
		if (f == NULL) {
			err("error creating log file, please run as root\n");
		}
	}

	va_start(ap, msg);

	for (retries = 0; retries < 3; retries++) {
		rval = vfprintf(f, msg, ap);
		if (rval < 0) {
			/* Log rotated, likely */
			fclose(f);
			f = fopen("/var/log/bigendian", "a+");
		} else {
			fprintf(f, "\n");
			fflush(f);
			break;
		}
	}	

	va_end(ap);
}