/*
 * Simple C Telegram Bot
 *
 * Angelo Dureghello (C) 2019 <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <stdbool.h>
#include <yaml.h>
#include "config.h"
#include "log.h"
#include "fs.h"

#define MAX_PATH_LEN		4096
#define CFG_FILE_NAME		"config.yaml"

#define SECT_CHANNELS		"channels"

#define ARG_REPLY		"reply"
#define ARG_FIND_CI		"ifind"

#define DEFAULT_CFG_PATH	"/etc/bigendian/"

static char *conf_file_places[] = {
	".",
	DEFAULT_CFG_PATH,
	0
};

enum states {
	s_start,
	s_channel_name,
	s_event_new,
	s_event_args,
	s_event_arg_reply,
	s_event_arg_ifind,
	s_event_seq,
};

struct context {
	int cur_e;
	int cur_c;
};

struct config cfg;

static struct context ctx;
static yaml_parser_t parser;

FILE * config_file_search()
{
	char buff[MAX_PATH_LEN] = {0};
	FILE *f;
	int i;

	logmsg("reading cfg file ...");

	for (i = 0; ; ++i) {
		if (conf_file_places[i]) {
			*buff = 0;
			strcpy(buff, conf_file_places[i]);
			strcat(buff, "/");
			strcat(buff, CFG_FILE_NAME);
			f = fopen(buff, "rb");
			if (f != NULL) {
				logmsg("config file found");
				return f;
			}
		}
	}

	logmsg("error reading config file");

	return NULL;
}

static int channel_enabled(char *channel)
{
	int i;

	for (i = 0; i < MAX_CHANNELS; ++i) {
		if (*cfg.channels[i].name == 0)
			break;
		if (strcmp(cfg.channels[i].name, channel) == 0)
			return i;
	}

	return -1;
}

static void config_add_channel(yaml_char_t *chan)
{
	strcat(cfg.channels[ctx.cur_c].name, (const char*)chan);

	dbg("yaml: new channel %s", cfg.channels[ctx.cur_c].name);
}

static void config_add_ch_event(yaml_char_t *event)
{
	int evt = EVT_NONE;

	if (strcmp((const char*)event, "evt_join") == 0)
		evt = EVT_JOIN;
	else if (strcmp((const char*)event, "evt_mesg") == 0)
		evt = EVT_MESG;

	cfg.channels[ctx.cur_c].evt[ctx.cur_e] = evt;

	dbg("yaml: new event %d", cfg.channels[ctx.cur_c].evt[ctx.cur_e]);
}

static bool config_evt_arg_is(yaml_char_t *arg_in, const char *arg)
{
	if (!strcmp((const char *)arg_in, arg))
		return true;

	return false;
}

static void config_add_event_reply(yaml_char_t *reply)
{
	strcat(cfg.channels[ctx.cur_c].act[ctx.cur_e].reply,
		(const char*)reply);

	dbg("yaml: new event-arg reply %s", (const char*)reply);
}

static void config_add_event_ifind(yaml_char_t *ifind)
{
	strcat(cfg.channels[ctx.cur_c].act[ctx.cur_e].ifind,
		(const char*)ifind);

	dbg("yaml: new event-arg ifind %s", (const char*)ifind);
}

static void config_add_allowed_id(yaml_char_t *id)
{
	static int table_idx = 0;

	if (table_idx >= MAX_OWNER_IDS)
		return;

	dbg("yaml: adding allowed id %s", (char*)id);

	cfg.allow_list[table_idx] = atoi((char *)id);

	table_idx++;
}

static void config_display_data()
{
	int c, e;

	for (c = 0; ; ++c) {
		if (*cfg.channels[c].name == 0)
			break;
		dbg("config: chan \"%s\"", cfg.channels[c].name);
		for (e = 0; ; e++) {
			if (cfg.channels[c].evt[e] == EVT_NONE)
				break;
			dbg("config: event: %d", cfg.channels[c].evt[e]);
			if (cfg.channels[c].act[e].reply)
				dbg("config: event: reply: [\"%s\"]",
					cfg.channels[c].act[e].reply);
			if (cfg.channels[c].act[e].ifind)
				dbg("config: event: ifind: [\"%s\"]",
				cfg.channels[c].act[e].ifind);
		}
	}
}

char * config_get_chatgroup_topic(char *title)
{
	int ch_id, e;

	if ((ch_id = channel_enabled(title)) != -1) {
		dbg("%s(): channel matched", __func__);
		for (e = 0; e < MAX_ACTIONS; e++) {
			if (cfg.channels[ch_id].evt[e] == EVT_NONE)
				break;
			if (cfg.channels[ch_id].evt[e] == EVT_JOIN) {
				dbg("%s(): event join  matched", __func__);
				return cfg.channels[ch_id].act[e].reply;
				break;
			}
		}
	}

	return 0;
}

int config_read_file()
{
	int state;
	bool mapping;
	FILE *file;

	file = config_file_search();
	if (!file) {
		logmsg("can't open config file, exiting.");
		return -1;
	}

	yaml_parser_initialize(&parser);
	yaml_parser_set_input_file(&parser, file);

	memset(&cfg, 0, sizeof(struct config));
	memset(&ctx, 0, sizeof(struct context));

	yaml_event_t  event;

	do {
		if (!yaml_parser_parse(&parser, &event)) {
			err("parser error %d\n", parser.error);
		}
		switch (event.type) {
		case YAML_NO_EVENT:
			break;
		/* Stream start/end */
		case YAML_STREAM_START_EVENT:
			dbg("config: yaml: stream start");
			break;
		case YAML_STREAM_END_EVENT:
			dbg("config: yaml: stream end");
			break;
		/* Block delimeters */
		case YAML_DOCUMENT_START_EVENT:
			dbg("config: yaml: start document");
			break;
		case YAML_DOCUMENT_END_EVENT:
			dbg("config: yaml: end document");
			break;
		case YAML_SEQUENCE_START_EVENT:
			dbg("config: yaml: start sequence");
			break;
		case YAML_SEQUENCE_END_EVENT:
			dbg("config: yaml: end sequence");
			break;
		case YAML_MAPPING_START_EVENT:
			mapping = true;
			dbg("config: yaml: start mapping");
			break;
		case YAML_MAPPING_END_EVENT:
			mapping = false;
			dbg("config: yaml: end mnapping");
			/* roll back */
			switch (state) {
			case s_event_args:
				state = s_event_new;
				ctx.cur_e++;
				break;
			case s_event_new:
				state = s_start;
				ctx.cur_c++;
				ctx.cur_e = 0;
			}
			break;
		/* Data */
		case YAML_ALIAS_EVENT:
			dbg("config: yaml: got alias (anchor %s)\n",
					event.data.alias.anchor);
			break;
		case YAML_SCALAR_EVENT:
			dbg("config: yaml: scalar event");
			switch (state) {
			case s_start:
				if (strcmp((const char*)
						event.data.scalar.value,
					"allow_list") == 0)
					state = s_event_seq;
				break;
			case s_channel_name:
				config_add_channel(event.data.scalar.value);
				state = s_event_new;
				break;
			case s_event_new:
				config_add_ch_event(event.data.scalar.value);
				state = s_event_args;
				break;
			case s_event_args:
				if (config_evt_arg_is(event.data.scalar.value,
					ARG_REPLY))
					state = s_event_arg_reply;
				if (config_evt_arg_is(event.data.scalar.value,
					ARG_FIND_CI))
					state = s_event_arg_ifind;
				break;
			case s_event_arg_reply:
				config_add_event_reply(event.data.scalar.value);
				state = s_event_args;
				break;
			case s_event_arg_ifind:
				config_add_event_ifind(event.data.scalar.value);
				state = s_event_args;
				break;
			case s_event_seq:
				config_add_allowed_id(event.data.scalar.value);
				break;
			}
			if (mapping) {
				mapping = 0;
				if (strcmp((const char*)event.data.scalar.value,
					SECT_CHANNELS) == 0)
					state = s_channel_name;
			}
			break;
		}
		if (event.type != YAML_STREAM_END_EVENT)
			yaml_event_delete(&event);
 	} while (event.type != YAML_STREAM_END_EVENT);

	yaml_parser_delete(&parser);
	fclose(file);

	config_display_data();

	return 0;
}

/*
 * Here we replace special tags, returning a modified message
 */
char result[1024 * 16];

char * config_special_tag_replace(struct new_chat_member *ncm, char *haystack)
{
	char st;
	char *ptr = haystack, *p = result, *pf, *field;
	int chunk_size;

	if (!ptr)
		return NULL;

	do {
		pf = strstr(ptr, "@@");
		if (!pf) {
			if (p == result)
				return haystack;

			strcpy(p, ptr);
			break;
		}

		chunk_size = (int)(pf - ptr);

		memcpy(p, ptr, chunk_size);
		p += chunk_size;
		pf += 2;
		st = *pf;

		switch (st) {
		case 'f':
			field = ncm->first_name;
			break;
		case 'l':
			field = ncm->last_name;
			break;
		case 'u':
			field = ncm->username;
			break;
		default:
			field = 0;
		}
		if (field) {
			strcpy(p, field);
			p += strlen(field);
		} else {
			strcpy(p, " ");
			p++;
		}
		ptr = pf;

	} while (ptr);

	return (p ? result : NULL);
}
