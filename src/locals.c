/*
 * Simple C Telegram Bot
 *
 * Angelo Dureghello (C) 2019 <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "locals.h"

void linux_command(char *linux_cmd, char *reply)
{
	/* simply invoke a app, pipe output*/
	static char reply_part[512];
	FILE *pipe;
	int clen;

	clen = strlen(linux_cmd);
	if (!clen) {
		sprintf(reply, "missing command");
		return;
	}

	pipe = popen(linux_cmd, "r");

	if (!pipe) {
		sprintf(reply, "error processing command");
		return;
	}

	sleep(1);

	/* TODO: protect */
	while (fgets(reply_part, 511, pipe) != NULL) {
		strcat(reply, reply_part);
	}

	/* Close pipe */
	pclose(pipe);
}