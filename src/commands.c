/*
 * Simple C Telegram Bot
 *
 * Angelo Dureghello (C) 2019 <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * 4as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <string.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <core.h>

#include "commands.h"
#include "locals.h"
#include "version.h"
#include "config.h"
#include "group.h"

extern struct chat_info group_ids[];

static inline char * get_cmd(struct update *u)
{
	return &u->msg->text[1];
}

static char *fetch(char *cmd_line_ptr, char *arg)
{
	char *p;

	if ((p = strchr(cmd_line_ptr, ' ')) != NULL) {
		*p = 0;

		if ((p - cmd_line_ptr) > MAX_ARG_LEN)
			return 0;

		strcpy(arg, cmd_line_ptr);

		return ++p;
	}

	return 0;
}

static int do_cmd_ping(struct update *u, char *reply, int64_t *dest_id)
{
	strcat(reply,  "i am alive.");

	return 0;
}

static int do_cmd_version(struct update *u, char *reply, int64_t *dest_id)
{
	strcat(reply, version);

	return 0;
}

static int do_cmd_linux(struct update *u, char *reply, int64_t *dest_id)
{
	char *cmd = get_cmd(u);

	if (!strchr(cmd, ' ')) {
		strcat(reply, "provide a linux command");
		return 0;
	}

	strcat(reply, "<pre>");
	linux_command(&cmd[6], &reply[5]);
	strcat(reply, "</pre>");

	return 0;
}

static int do_cmd_get_ids(struct update *u, char *reply, int64_t *dest_id)
{
	int i = 0;

	strcat(reply, "<pre>");

	for (; i < MAX_CHANNELS; ++i) {
		if (group_ids[i].id == 0)
			break;
		sprintf(&reply[5], "%d %" PRId64 "   ", i, group_ids[i].id);
		strcat(reply, group_ids[i].title);
		strcat(reply, "\n");
	}

	if (i == 0)
		strcat(reply, "no entries.");

	strcat(reply, "</pre>");

	return 0;
}

static int do_cmd_write(struct update *u, char *reply, int64_t *dest_id)
{
	char tmp[MAX_ARG_LEN + 1];
	char *cur;
	char *cmd = get_cmd(u);

	/* First argument is table idx */
	cur = fetch(cmd, tmp);
	if (!cur)
		return -1;

	*dest_id = group_get_id(atoi(tmp));

	cur = fetch(cur, tmp);
	if (!cur)
		return -1;

	strcat(reply, cur);

	return 0;
}

/* Chatgroup commands */

static int do_cmd_topic(struct update *u, char *reply, int64_t *dest_id)
{
	char * msg = config_get_chatgroup_topic(u->msg->chat->title);

	/* TODO : in reply to */
	if (msg) {
		struct new_chat_member ncm = {0};
		char *rep;

		rep = config_special_tag_replace(&ncm, msg);

		strncpy(reply, rep, strlen(rep));

		return 0;
	}

	return 1;
}

static int do_cmd_ban(struct update *u, char *reply, int64_t *dest_id)
{
	if (!u->msg->reply_to)
		return 1;

	if (!u->msg->reply_to->from->id)
		return 1;

	be_kick_chat_member(u->msg->chat->id, u->msg->reply_to->from->id);

	return 1;
}

/* command, enabled, callback */
const struct command cmds_pvt[] = {
	{"ping", do_cmd_ping, CF_ENABLED},
	{"version", do_cmd_version, CF_ENABLED},
	{"linux", do_cmd_linux, CF_ENABLED | CF_USER_CHECK | CF_HTML_REPLY},
	{"get_ids", do_cmd_get_ids, CF_ENABLED | CF_USER_CHECK | CF_HTML_REPLY},
	{"write", do_cmd_write, CF_ENABLED | CF_USER_CHECK},
	{ 0 },
};

/* command, enabled, callback */
const struct command cmds_grp[] = {
	{"topic", do_cmd_topic, CF_ENABLED | CF_HTML_REPLY},
	{"ban", do_cmd_ban, CF_ENABLED | CF_USER_CHECK},
	{ 0 },
};

