/*
 * Simple C Telegram Bot
 *
 * Angelo Dureghello (C) 2019 <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along https://gibiru.com/with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <core.h>

#include "tools.h"
#include "log.h"
#include "fs.h"
#include "config.h"
#include "commands.h"
#include "version.h"
#include "group.h"

extern struct config cfg;

#define TOUT_SLEEP_S		3

int init_bot()
{
	char token[MAX_TOKEN];

	dbg("%s(): entering", __func__);

	if (be_init() != 0)
		return -1;

	dbg("%s(): be_init ok", __func__);

	if (fs_get_token(token)) {
		logmsg("%s(): cannot read token, exiting", __func__);
		return -1;
	}

	be_setup_token(token);

	return 0;
}

static void on_new_member(struct update *u)
{
	char *msg;

	dbg("%s():\n chat-id:%d\n username:%s title[%s]",
	 	__func__,
		u->msg->chat->id,
		u->msg->chat->username,
		u->msg->chat->title);

	msg = config_get_chatgroup_topic(u->msg->chat->title);

	/* TODO : in reply to */
	if (msg) {
		char *rep = config_special_tag_replace
				(u->msg->new_chat_member, msg);

		be_send_message_in_reply_to(
				u->msg->chat->id, u->msg->id, rep, 1);
	}
}

static const char *dict_volgarita[] = {
	"merda",
	"cazzo",
	"coglione",
	"stronzo",
	"merdoso",
	0,
};

static const char *dict_politica[] = {
	"comunista",
	"berlusconi",
	"salvini",
	"partito democratico",
	"giorgia meloni",
	"trump",
	0,
};

static const char warning_volgarita[] = {
	"Hey, this language is not welcome. Be careful .."};

static const char warning_politica[] = {
	"Hey, i heard something about politics, please stop .."};

static void dict_check(struct update *u, const char **dict, const char *warn)
{
	char *msg = u->msg->text;
	int check = 0;

	if (!msg)
		return;

	for (;;) {
		if (dict[check] == 0 || *dict[check] == 0)
			break;

		if (strstr(msg, dict[check])) {
			be_send_message_in_reply_to(
				u->msg->chat->id, u->msg->id, (char*)warn, 0);

			return;
		}
		check++;
	}
}

static int allowed_user(int64_t id)
{
	int i = 0;

	for (; i < MAX_OWNER_IDS; ++i ) {
		if (cfg.allow_list[i] == id)
			return 1;

		if (cfg.allow_list[++i] == 0)
			break;
	}

	return 0;
}

static int cmd_smurf_detected(char *cmd)
{
	int i;

	for (i = 0; i < MAX_CMD_LEN; ++i) {
		if (cmd[i] == ' ' || cmd[i] == 0)
			return 0;
	}

	if (i >= MAX_CMD_LEN || strlen(cmd) > MAX_CMD_ARGS_LEN) {
		logmsg("%s(): smurf attempt, i %d len %d",
		    __func__, i, strlen(cmd));
		logmsg("%s(): cmd %s",
		    __func__, cmd);
		return 1;
	}

	return 0;
}

static void on_chatgroup_cmd(struct update *u)
{
	static char cmd_reply[4096];
	int idx = 0, len, html = 0;
	char *cmd = &u->msg->text[1];
	int64_t dest_id = u->msg->chat->id;

	*cmd_reply = 0;

	/* Anti smurf */
	if (cmd_smurf_detected(cmd)) {
		be_send_message(dest_id, "hey, fuck you !", 0);
		return;
	}

	for (;;) {
		/* muting */
		if (!cmds_grp[idx].cmd)
			return;

		len = strlen(cmds_grp[idx].cmd);

		if (strncmp(cmds_grp[idx].cmd, cmd, len) == 0) {
			/* Some safe checks */
			if (!cmds_grp[idx].cb)
				return;
			if (!(cmds_grp[idx].flags & CF_ENABLED))
				return;
			if (cmds_grp[idx].flags & CF_USER_CHECK) {
				if (!allowed_user(u->msg->from->id))
					return;
			}
			if (cmds_grp[idx].cb(u, cmd_reply, &dest_id))
				return;
			if (cmds_grp[idx].flags & CF_HTML_REPLY)
					html = 1;

			break;
		}
		idx++;
	}

	be_send_message(dest_id, cmd_reply, html);
}

static void on_chatgroup_message(struct update *u)
{
	dict_check(u, dict_volgarita, warning_volgarita);
	dict_check(u, dict_politica, warning_politica);

	if (!u->msg->text)
		return;

	if (u->msg->text[0] == '/')
		on_chatgroup_cmd(u);
}

static void on_message_from_chatgroup(struct update *u)
{
	dbg("%s():\n chat-id:%d\n title:%s\n username:%s\n msg:%s",
	 	__func__,
		u->msg->chat->id,
		u->msg->chat->title,
		u->msg->chat->username,
		u->msg->text);

	group_update_id_table(u->msg->chat->id, u->msg->chat->title);

	if (u->msg->new_chat_member)
		on_new_member(u);
	else
		on_chatgroup_message(u);
}

static void on_bot_command(struct update *u)
{
	static char cmd_reply[4096];
	int idx = 0, len, html = 0;
	char *cmd = &u->msg->text[1];
	int64_t dest_id = u->msg->chat->id;;

	*cmd_reply = 0;

	/* Anti smurf */
	if (cmd_smurf_detected(cmd)) {
		be_send_message(dest_id, "hey, fuck you !", 0);
		return;
	}

	for (;;) {
		if (!cmds_pvt[idx].cmd) {
			strcat(cmd_reply, "++err: no such command available");
			break;
		}

		len = strlen(cmds_pvt[idx].cmd);

		if (strncmp(cmds_pvt[idx].cmd, cmd, len) == 0) {
			/* Some safe checks */
			if (!cmds_pvt[idx].cb)
				return;
			if (!(cmds_pvt[idx].flags & CF_ENABLED))
				return;
			if (cmds_pvt[idx].flags & CF_USER_CHECK) {
				int64_t from_id = u->msg->from->id;

				dbg("%s(): id %d\n",
					__func__, from_id);
				if (!allowed_user(from_id)) {
					sprintf(cmd_reply, "id: [%ld] ",
						from_id);
					strcat(cmd_reply,
					       "you are not allowed");
					break;
				}
			}
			if (cmds_pvt[idx].cb(u, cmd_reply, &dest_id))
				strcpy(cmd_reply, "++err: syntax error");
			else
				if (cmds_pvt[idx].flags & CF_HTML_REPLY)
					html = 1;

			break;
		}
		idx++;
	}

	be_send_message(dest_id, cmd_reply, html);
}

static void on_message_to_bot(struct update *u)
{
	dbg("%s(): %s", __func__, u->msg->text);

	if (u->msg->chat->id) {
		if (!u->msg->text)
			return;
		if (u->msg->text[0] == '/')
			on_bot_command(u);
	}
}

/*
 * NOTE: bot must be admin in the group, or no message from
 * the group will be received.
 */
static void on_message(struct update *u)
{
	dbg("%s(): entering, id %d chat id %ld", __func__,
	    u->msg->id, u->msg->chat->id);

	if (u->msg->chat) {
		if (! *u->msg->chat->title)
			on_message_to_bot(u);
		else
			on_message_from_chatgroup(u);
	}
}

static int engine()
{
	int rval;
	struct update root, *u;

	u = &root;

	dbg("%s(): engine start", __func__);

	for (;;) {
		sleep(TOUT_SLEEP_S);

		u = &root;
		memset(u, 0, sizeof(struct update));

		rval = be_get_updates(u);
		if (!rval) {
			if (u->result != 1)
				continue;

			/* Process all updates */
			while (u) {
				dbg("%s() update id: %d",
					__func__, u->update_id);

				if (u->msg)
					on_message(u);

				u = u->next;
			}
			/* And cleanup */
			be_cleanup_updates(&root);
		}
	}

	return rval;
}

int run()
{
	if (config_read_file()) {
		logmsg("bigendian bot failed to read config, exiting.");
		return -1;
	}

	if (init_bot() != 0) {
		logmsg("bigendian bot failed to start, exiting.");
		return -1;
	}

	logmsg("bot initialization completed, starting ...");

	return engine();
}
