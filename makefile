# bigendian makefile

CC=gcc

BIN=stbot

# libraries
LIBS=-lbeapi -ljson-c -lcurl -lyaml

library=beapi

# relative paths
SRCDIR=src
INCDIR=include
OBJDIR=obj
BINDIR=.

SRCS:=$(wildcard $(SRCDIR)/*.c)
OBJS:=$(patsubst %.c,%.o,$(SRCS))
OBJS:=$(patsubst $(SRCDIR)%,$(OBJDIR)%,$(OBJS))

CFLAGS=-c -pipe -fomit-frame-pointer -Wall -O0 -ggdb -std=c99 -D_XOPEN_SOURCE=500

.PHONY: all $(library)

all: $(BINDIR)/$(BIN)

$(library):
	$(MAKE) --directory=$@

$(BINDIR)/$(BIN): $(OBJS) $(library)
	$(CC) -Lbeapi $(OBJS) $(LIBS) -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)
	$(CC) -I$(INCDIR) -Ibeapi/include $(CFLAGS) $< -o $@

$(OBJDIR):
	mkdir -p $(OBJDIR)

clean:
	rm $(OBJS)
	rm $(BIN)
